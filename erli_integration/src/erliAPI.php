<?php

use Hough\Guzzle\Client;
use Hough\Guzzle\Exception\GuzzleException;

class ErliAPI
{
    const BEARER_KEY = 'Bearer r+CUjgUZoQNY:jpn0BnrVfl6pmINR';
    const LOG = 1;
    const ENV = 'https://sandbox.erli.dev/svc/shop-api/';

    private $client;

    public function __construct()
    {
        $this->client = new Client;
    }

    /**
     * Add product to market
     *
     * @param int $product_id - product id
     * @param array $product_data - product data
     *
     * @return string
     */
    public function addProduct($product_id, $product_data)
    {
        try {
            $response = $this->client->request(
                'POST',
                self::ENV . 'products/' . $product_id,
                array(
                    'headers' => self::setHeader(),
                    'json' => $product_data
                )
            );
        } catch (GuzzleException $e) {
            $message = $e->getMessage();
            $code = $e->getCode();
            $this->logEvent('ProductAdd: ' . $product_id . '  Status: ' . $code . '___ Message:' . $message);
            return "Code: " . $code . " Message: " . $message;
        }

        $status_code = $response->getStatusCode();
        $this->logEvent('Product: ' . $product_id . ' Status: ' . $status_code);
        return $status_code;
    }

    /**
     * Get product from market
     *
     * @param int $product_id
     */
    public function getProduct($product_id)
    {
        try {
            $response = $this->client->request(
                'GET',
                self::ENV . 'products/' . $product_id,
                array(
                    'headers' => self::setHeader()
                )
            );
        } catch (GuzzleException $e) {
            $message = $e->getMessage();
            $code = $e->getCode();
            return "Code: " . $code . " Message: " . $message;
        }

        return $response->getBody()->getContents();
    }

    /**
     * Update product
     *
     * @param int $product_id
     * @param array $product_data
     * @return string
     */
    public function updateProduct($product_id, $product_data)
    {
        try {
            $response = $this->client->request(
                'PATCH',
                self::ENV . 'products/' . $product_id,
                array(
                    'headers' => self::setHeader(),
                    'json' => $product_data,
                )
            );
        } catch(GuzzleException $e) {
            $message = $e->getMessage();
            $code = $e->getCode();
            $this->logEvent('ProductUpdate: ' . $product_id . '  Status: ' . $code . '___ Message:' . $message);
            return "Code: " . $code . " Message: " . $message;
        }

        $status_code = $response->getStatusCode();
        $this->logEvent('Product: ' . $product_id . ' Status: ' . $status_code);
        return $status_code;
    }

    public function setHeader()
    {
        return array(
            'Accept' => 'application/json',
            'User-Agent' => 'SuperShop v8.8 (marketplace-plugin v1.2)',
            'Authorization' => self::BEARER_KEY,
        );
    }

    public function logEvent($event)
    {
        if (self::LOG) {
            file_put_contents('./logs/log_' . date("j.n.Y") . '.log', $event . "\n", FILE_APPEND);
        }
    }
}
