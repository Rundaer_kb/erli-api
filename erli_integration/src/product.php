<?php

class Product
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getSampleData()
    {
        return array(
            'name' => 'Zmywarka testowa',
            'description' => '<h1>Zmywarka nie z tej ziemi!!!</h1>',
            'images' => array(
                0 => array(
                    'url' => 'https://f01.esfr.pl/foto/9/72207329817/70308e0e6d272664a475cd2db78129ec/whirlpool-wfc-3c33-pf-x,72207329817_5.jpg',
                ),
            ),
            'price' => 300000, # Cena podawana w groszach
            'stock' => 10,
            'status' => 'active', # /inactive - status produktu
            'dispatchTime' => array(
                'period' => 1,
                'unit' => 'hour',
            ),
            'packaging' => array(
                'tags' => array('DHL') # Nazwa przewoźnika z cennika dostaw
            ),
            'externalCategories' => array( # Kategorie produktu w Twoim sklepie, później mapowane w panelu
                0 => array(
                    'breadcrumb' => array(
                        0 => array(
                            'id' => '64a52abb-7d1a-4a06-a1bf-9c24320c8830',
                            'name' => 'Elektronika',
                        ),
                        1 => array(
                            'id' => '0ac260e3-e104-4816-9636-8774e92371e2',
                            'name' => 'AGD',
                        ),
                    ),
                    'index' => 0,
                ),
            ),
            'externalAttributes' => array(
                0 => array(
                    'id' => $this->id.'-zmywarka-wirpol',
                    'name' => 'Kolor',
                    'source' => 'shop',
                    'type' => 'number',
                    'index' => 0,
                    'values' => array(
                        0,
                    ),
                    'unit' => 'string',
                ),
                1 => array(
                    'id' => $this->id.'-zmywarka-wirpol',
                    'name' => 'string',
                    'source' => 'shop',
                    'type' => 'range',
                    'index' => 0,
                    'values' => array(
                        'from' => 0,
                        'to' => 0,
                    ),
                    'unit' => 'string',
                ),
            ),
            'externalVariantGroup' => array(
                'id' => 'string',
                'source' => 'integration',
                'attributes' => array(
                    0 => 'thumbnail',
                    1 => 0,
                ),
            ),
        );
    }
}
