<?php

//Potrzebujemy więc:
//- edycja ceny na istniejącej aukcji wraz z info zwrotnym czy się powiodło
//- aktywuj / dezaktywuj aukcję wraz z info zwrotnym czy się powiodło
//Opcjonalnie:
//- utwórz nową aukcję i przechwyć jej numer lub komunikat o błędzie

require 'erli_integration/vendor/autoload.php';
require 'erli_integration/src/erliAPI.php';
require 'erli_integration/src/product.php';

$erliApi = new ErliAPI();

// Testowy produkt
$product = new Product(1003);
$id = $product->id;
$data = $product->getSampleData();

# Dodawanie pojedynczego produktu
echo "Begin adding product \r\n";
$resultAdd = $erliApi->addProduct($id, $data);
echo $resultAdd . "\n";

# Pobieranie informacji o pojedynczym produkcie
//echo "Begin get info about product \r\n";
//$resultGet = $erliApi->getProduct($id);

# Edytowanie danych wybranego produktu
//$productEdit = [
//    'status' => 'active',
//    'price' => 2000
//];
//
//echo "Begin edit product price \n";
//$resultEdit = $erliApi->updateProduct($id, $productEdit);
//echo $resultEdit;
